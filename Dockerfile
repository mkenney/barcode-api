FROM golang:1.9-alpine

RUN set -x \
    && apk update \
    && apk upgrade \
    && apk add git \
    && go get -u github.com/golang/dep/cmd/dep

EXPOSE 80
COPY ./src /go/src/gitlab.com/Littrell/barcode-api/src

RUN set -x \
    && cd /go/src/gitlab.com/Littrell/barcode-api/src \
    && dep ensure \
    && go build -o /go/bin/app

ENTRYPOINT ["/go/bin/app"]
