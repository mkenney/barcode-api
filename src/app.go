package main

import (
	"fmt"

	"gitlab.com/Littrell/barcode-api/src/api"
)

func main() {
	fmt.Println("Running on port 80")
	a := api.New()
	a.Run(":80")
}
