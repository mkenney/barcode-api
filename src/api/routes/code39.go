package routes

import (
	"net/http"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/code39"
)

func Code39GET(w http.ResponseWriter, r *http.Request, text string, height int, width int) (fileName string, err error) {
	var code barcode.Barcode

	// TODO fill in these two as parameters
	// first parameter is includeChecksum bool
	// second parameter is fullASCIIMode bool
	code, err = code39.Encode(text, false, false)

	fileName, err = renderCode(code, width, height)

	return
}

func Code39OPTIONS(w http.ResponseWriter, r *http.Request) (optionsString string) {
	optionsString = "Hey, hit the Code39 OPTIONS route"
	return
}
