package routes

import (
	"image/png"
	"io/ioutil"
	"os"

	"github.com/boombuler/barcode"
)

// TODO need an error return type
// TODO need a return type
func renderCode(code barcode.Barcode, width int, height int) (fileName string, err error) {
	// Scale bar code
	code, err = barcode.Scale(code, width, height)
	if err != nil {
		return "There was an error while scaling bar code", err
	}

	// Save PNG to a temp file
	file, err := ioutil.TempFile(os.TempDir(), "code")
	if err != nil {
		return "There was an error preparing file", err
	}

	// Create final render
	if err := png.Encode(file, code); err != nil {
		return "There was an error encoding final image", err
	}

	return file.Name(), nil
}
