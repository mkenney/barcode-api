package routes

import (
	"net/http"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/datamatrix"
)

func DatamatrixGET(w http.ResponseWriter, r *http.Request, text string, height int, width int) (fileName string, err error) {
	var code barcode.Barcode

	code, err = datamatrix.Encode(text)

	fileName, err = renderCode(code, width, height)

	return
}

func DatamatrixOPTIONS(w http.ResponseWriter, r *http.Request) (optionsString string) {
	optionsString = "Hey, hit the Datamatrix OPTIONS route"
	return
}
