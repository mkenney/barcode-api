package routes

import (
	"net/http"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/aztec"
)

func AztecGET(w http.ResponseWriter, r *http.Request, text string, height int, width int) (fileName string, err error) {
	var code barcode.Barcode

	code, err = aztec.Encode([]byte(text), aztec.DEFAULT_EC_PERCENT, aztec.DEFAULT_LAYERS)

	fileName, err = renderCode(code, width, height)

	return
}

func AztecOPTIONS(w http.ResponseWriter, r *http.Request) (optionsString string) {
	optionsString = "Hey, hit the Aztec OPTIONS route"
	return
}
