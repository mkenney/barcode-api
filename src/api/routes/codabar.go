package routes

import (
	"net/http"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/codabar"
)

func CodabarGET(w http.ResponseWriter, r *http.Request, text string, height int, width int) (fileName string, err error) {
	var code barcode.Barcode

	code, err = codabar.Encode(text)

	fileName, err = renderCode(code, width, height)

	return
}

func CodabarOPTIONS(w http.ResponseWriter, r *http.Request) (optionsString string) {
	optionsString = "Hey, hit the Codabar OPTIONS route"
	return
}
