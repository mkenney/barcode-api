package routes

import (
	"net/http"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/ean"
)

func EanGET(w http.ResponseWriter, r *http.Request, text string, height int, width int) (fileName string, err error) {
	var code barcode.Barcode

	code, err = ean.Encode(text)

	fileName, err = renderCode(code, width, height)

	return
}

func EanOPTIONS(w http.ResponseWriter, r *http.Request) (optionsString string) {
	optionsString = "Hey, hit the Ean OPTIONS route"
	return
}
