package routes

import (
	"net/http"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/code128"
)

func Code128GET(w http.ResponseWriter, r *http.Request, text string, height int, width int) (fileName string, err error) {
	var code barcode.Barcode

	code, err = code128.Encode(text)

	fileName, err = renderCode(code, width, height)

	return
}

func Code128OPTIONS(w http.ResponseWriter, r *http.Request) (optionsString string) {
	optionsString = "Hey, hit the Code128 OPTIONS route"
	return
}
