package routes

import (
	"net/http"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/pdf417"
)

func Pdf417GET(w http.ResponseWriter, r *http.Request, text string, height int, width int) (fileName string, err error) {
	var code barcode.Barcode

	// TODO add second parameter
	// second parameter is securityLevel byte
	// ranges between 0 and 8
	code, err = pdf417.Encode(text, 0)

	fileName, err = renderCode(code, width, height)

	return
}

func Pdf417OPTIONS(w http.ResponseWriter, r *http.Request) (optionsString string) {
	optionsString = "Hey, hit the Pdf417 OPTIONS route"
	return
}
