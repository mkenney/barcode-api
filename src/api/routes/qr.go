package routes

import (
	"errors"
	"net/http"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/qr"
)

func QrGET(w http.ResponseWriter, r *http.Request, text string, height int, width int) (fileName string, err error) {
	var code barcode.Barcode
	var errorCorrectionLevel qr.ErrorCorrectionLevel

	errorCorrectionLevelPct := r.FormValue("errorCorrectionLevelPct")

	if errorCorrectionLevelPct == "" || errorCorrectionLevelPct == "15" {
		errorCorrectionLevel = qr.M
	} else if errorCorrectionLevelPct == "7" {
		errorCorrectionLevel = qr.L
	} else if errorCorrectionLevelPct == "25" {
		errorCorrectionLevel = qr.Q
	} else if errorCorrectionLevelPct == "30" {
		errorCorrectionLevel = qr.H
	} else {
		err = errors.New("Error correct level percentage not supported.")
		return
	}

	/*
		qr.Auto is the Auto encoding type;
		this also allows for Numeric, AlphaNumeric, and Unicode
	*/
	code, err = qr.Encode(text, errorCorrectionLevel, qr.Auto)

	fileName, err = renderCode(code, width, height)

	return
}

func QrOPTIONS(w http.ResponseWriter, r *http.Request) (optionsString string) {
	optionsString = "Hey, hit the QR OPTIONS route"
	return
}
