package routes

import (
	"net/http"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/twooffive"
)

func TwooffiveGET(w http.ResponseWriter, r *http.Request, text string, height int, width int) (fileName string, err error) {
	var code barcode.Barcode

	// TODO add 'interleaved' param
	// Second parameter is 'interleaved'
	code, err = twooffive.Encode(text, false)

	fileName, err = renderCode(code, width, height)

	return
}

func TwooffiveOPTIONS(w http.ResponseWriter, r *http.Request) (optionsString string) {
	optionsString = "Hey, hit the Twooffive OPTIONS route"
	return
}
