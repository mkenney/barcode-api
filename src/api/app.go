package api

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.com/Littrell/barcode-api/src/api/routes"
)

type App struct {
	router  *mux.Router
	get     map[string]interface{}
	options map[string]interface{}
}

func New() *App {
	a := new(App)
	a.router = mux.NewRouter()

	a.router.HandleFunc("/", a.Index).Methods("GET")
	a.router.HandleFunc("/types", a.Types).Methods("GET")
	a.router.HandleFunc("/types/{type}", a.TypeGET).Methods("GET")
	a.router.HandleFunc("/types/{type}", a.TypeOPTIONS).Methods("OPTIONS")

	// TODO might be able to use the constants here: https://godoc.org/github.com/boombuler/barcode
	// TODO use reflection to create this map
	a.get = map[string]interface{}{
		"qr":         routes.QrGET,
		"aztec":      routes.AztecGET,
		"datamatrix": routes.DatamatrixGET,
		"twooffive":  routes.TwooffiveGET,
		"codabar":    routes.CodabarGET,
		"ean":        routes.EanGET,
		"code128":    routes.Code128GET,
		"code39":     routes.Code39GET,
		"code93":     routes.Code93GET,
		"pdf417":     routes.Pdf417GET,
	}

	// TODO use reflection to create this map
	a.options = map[string]interface{}{
		"qr":         routes.QrOPTIONS,
		"aztec":      routes.AztecOPTIONS,
		"datamatrix": routes.DatamatrixOPTIONS,
		"twooffive":  routes.TwooffiveOPTIONS,
		"codabar":    routes.CodabarOPTIONS,
		"ean":        routes.EanOPTIONS,
		"code128":    routes.Code128OPTIONS,
		"code39":     routes.Code39OPTIONS,
		"code93":     routes.Code93OPTIONS,
		"pdf417":     routes.Pdf417OPTIONS,
	}

	return a
}

func (a *App) Run(addr string) {
	log.Fatal(http.ListenAndServe(addr, a.router))
}

///////////////////////////////////////////
// ROUTE HANDLERS
///////////////////////////////////////////

// GET /index
func (a *App) Index(w http.ResponseWriter, r *http.Request) {
	RespondWithJSON(w, 200, "Try /types...")
}

// OPTIONS /types/{type}
func (a *App) TypeOPTIONS(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	codeType := vars["type"]

	if codeType == "" {
		codeType = "qr"
	}

	if !StringInMap(codeType, a.get) {
		types := GetMapKeys(a.get)
		RespondWithError(w, http.StatusBadRequest, fmt.Sprintf("%s is an invalid type. Please provide one of the following types: %s", codeType, strings.Join(types, ", ")))
		return
	}

	funcReturns, _ := Call(a.options, codeType, w, r)
	options := funcReturns[0].String()

	RespondWithJSON(w, 200, options)
}

// GET /types/{type}
func (a *App) TypeGET(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)

	codeType := vars["type"]
	if codeType == "" {
		codeType = "qr"
	}

	if !StringInMap(codeType, a.get) {
		types := GetMapKeys(a.get)
		RespondWithError(w, http.StatusBadRequest, fmt.Sprintf("%s is an invalid type. Please provide one of the following types: %s", codeType, strings.Join(types, ", ")))
		return
	}

	text := r.FormValue("text")
	if text == "" {
		RespondWithError(w, http.StatusBadRequest, "Must provide text to convert!")
		return
	}

	width, err := strconv.Atoi(r.FormValue("width"))
	if err != nil {
		width = 200
	}

	height, err := strconv.Atoi(r.FormValue("height"))
	if err != nil {
		height = 200
	}

	funcReturns, err := Call(a.get, codeType, w, r, text, height, width)
	if err != nil {
		RespondWithError(w, http.StatusInternalServerError, fmt.Sprintf("There was an issue processing this request. Contact the developer. %s", err))
		return
	}
	filename := funcReturns[0].String()

	// Send the temp file
	http.ServeFile(w, r, filename)

	// Remove temp file
	defer os.Remove(filename)

	return
}

func (a *App) Types(w http.ResponseWriter, r *http.Request) {
	types := GetMapKeys(a.get)
	RespondWithJSON(w, 200, types)
}
